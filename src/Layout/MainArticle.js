import React,{ Component } from 'react'
import data from "../entertainment.json";

export class MainArticle extends Component {     
  render() {
    return(
      <div>
        <h2 className="underline">Food & Health</h2>
        <div className="main-layout">
          <div class="col col-main" role="main">
            <img src="https://place-hold.it/600x200/b8b2b8/ffffff.png" className="img-res"/>            
            <h2 className="main-headline">{this.props.single.story.headline}</h2>
            <small className="text-author text-author-main">{this.props.single.story["author-name"]}</small>
          </div>
          <div class="col col-side" role="complementary">
              {this.props.all.items.map((item,key) => {
                return(
                  <div className="box-side">
                    <h4>{item.story ? item.story.headline : "" }</h4>
                    <small className="text-author">{item.story ? item.story["author-name"] : ""}</small>
                  </div>
                )                
              })}                                     
          </div> 
        </div>        
      </div>
    )
  }
}