import React,{ Component } from 'react';

export class BoxArticle extends Component {
  render() {
    console.log(this.props.cardcontent)
    return(      
      <div id="card-layout">
        {this.props.cardcontent.map((item,key) => {
          return(
            <div class="card">
              <img src="https://place-hold.it/100x50/b8b2b8/ffffff.png" className="img-res" />
              <h3>{item.story ? item.story.headline : "" }</h3>
              <p className="text-author">{item.story ? item.story["author-name"] : ""}</p>
            </div>
          )          
        })}                        
        <div class="card sub-card">
          {this.props.subcontent.map((item,key) => {
            return(
                <div className="sub-content-article">                
                  <h3>{item.story ? item.story.headline : "No Headline" }</h3>
                  <p className="text-author">{item.story ? item.story["author-name"] : "NO Author"}</p>
                </div>
            )          
          })} 
        </div>
        <div class="card sub-card">
          {this.props.subcontent.map((item,key) => {
            return(
                <div className="sub-content-article">                
                  <h3>{item.story ? item.story.headline : "No Headline" }</h3>
                  <p className="text-author">{item.story ? item.story["author-name"] : "No Author"}</p>
                </div>
            )          
          })} 
        </div>
      </div>      
    )
  }
}