import React,{ Component } from 'react';
import data from "../entertainment.json";
import { MainArticle } from '../Layout/MainArticle'
import { SlideArticle } from '../Layout/SlideArticle'
import { BoxArticle } from '../Layout/BoxArticle'
import { BlogArticle } from '../Layout/BlogArticle'

export class Layout extends Component {
  constructor(props) {
    super(props);
    this.state={
      data
    }
  } 
  
  render() {    
    return (      
      <div>
           <div className="layout-main">
          <MainArticle all={this.state.data} single={this.state.data.items[0]} />
        </div>
        <div className="layout-slide">
          <SlideArticle all={this.state.data} />
        </div>
        <div className="layout-box">
          <BoxArticle cardcontent={this.state.data.items.slice(0,4)} subcontent={this.state.data.items.slice(4,7)} />
        </div>
        <div className="layout-blog">
          <BlogArticle blogcontent={this.state.data.items.slice(0,3)} />
        </div>   

      </div>
    )
  }
}