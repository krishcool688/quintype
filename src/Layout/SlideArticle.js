import React, { Component } from 'react'
import Slider from "react-slick";


const SampleNextArrow = props => {
	const { onClick } = props;
	return (
		<div className="tile-slider-arrows tile-slider-arrow-next" onClick={onClick}>
			<i className="fas fa-chevron-right fa-2x"></i>
		</div>
	);
};

const SamplePrevArrow = props => {
	const { onClick } = props;
	return (
		<div className="tile-slider-arrows tile-slider-arrow-prev" onClick={onClick}>
			<i className="fas fa-chevron-left fa-2x"></i>
		</div>
	);
};

export class SlideArticle extends Component {
  render() {
    const settings = {
      className: "center",
      centerMode: true,
      infinite: true,            
      slidesToShow: 3,
      speed: 500,
      arrows: true,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    return(
      <div>
        <Slider {...settings}>
          {this.props.all.items.map((item,key) => {
            return(
              <div className="wrapper">
                <img src="https://place-hold.it/150x100/b8b2b8/ffffff.png" className="img-res" />
                <div className="text"><h3>{item.story ? item.story.headline : "" }</h3></div>
              </div>
            )
          })}          
        </Slider>
      </div>
    )
  }
}