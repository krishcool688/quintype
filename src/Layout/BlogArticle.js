import React,{ Component } from 'react'
import timeSince from '../utils/timeconvert'
import resimage from '../assets/publish.png'

export class BlogArticle extends Component {
  render() {
    return (
      <div className="main-layout">
        <div class="col col-main-blog" role="main">
          {/* <img src="https://dummyimage.com/600x200/b8b2b8/ffffff.png" className="img-res"/>            
          <h2>{this.props.single.story.headline}</h2>
          <small className="text-author">{this.props.single.story["author-name"]}</small> */}
          <div className="blog-container">
            {this.props.blogcontent.map((item,key) => {
              return(
                <div className="blog-content">
                  <img src="https://place-hold.it/250x250/b8b2b8/ffffff.png" className="img-res blog-img"/>
                  <span className="blog-headline">
                    <h2 className="main-headline">{item.story ? item.story.headline : "No Headline" }</h2>                                        
                    <p className="text-author text-author-main">Published on {item.story ? item.story["author-name"] : "No Author"}</p>
                    <p className="text-author text-author-main">Published on {item.story ? new Date(item.story["published-at"]).toLocaleDateString("en-US") : "No Date"}</p>
                  </span>
                </div>
              )
            })}            
          </div>
        </div>
        <div class="col col-side-blog" role="complementary">
            <div className="publish-box">
              <h2><b>Publish with Quintype</b></h2>
              <small>Mobile first,Social first,Data first</small>
              <img src={resimage} />
              <small>www.quintype.com</small>
            </div>                     
        </div>
      </div>
    )
  }
}