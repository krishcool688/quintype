import React, { Component } from 'react';
import { Header } from './common/Header'
import { Footer } from './common/Footer'
import { Layout } from './Layout/Layout'
import './App.css'

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Layout />
        <Footer />
      </div>
    );
  }
}

export default App;
