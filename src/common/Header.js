import React, { Component } from 'react'

export class Header extends Component {    
  constructor() {
		super();
		this.state = {
			shown: true,
		};
  }	
  
  toggle() {
		this.setState({
			shown: !this.state.shown
		});
  }
  

  render() {      
    var shown = {
			display: this.state.shown ? "block" : "none"
		};
		
		var hidden = {
			display: this.state.shown ? "none" : "block"
    }
    console.log(this.state.shown)

    return(
      <nav className="navbar">
        <span className="navbar-toggle" id="js-navbar-toggle" onClick={this.toggle.bind(this)}>
            <i className="fas fa-bars"></i>
        </span>
        <a href="#" className="logo">QuinType</a>
        <ul className={this.state.shown ? "main-nav" : "main-nav-hide"} id="js-menu">
            <li>
                <a href="#" className="nav-link">Business</a>
            </li>
            <li>
                <a href="#" className="nav-link">Politics</a>
            </li>
            <li>
                <a href="#" className="nav-link">Entertainment</a>
            </li>
            <li>
                <a href="#" className="nav-link">World</a>
            </li>
            <li>
                <a href="#" className="nav-link">Signin</a>
            </li>
        </ul>
      </nav>
    )
  }
}

