import React, { Component } from 'react'

export class Footer extends Component {
  render() {
    return(
      <div className="footer">
        <div className="left-footer">
          Quintype
        </div>
        <div className="right-footer">
          <ul>
            <li>About us</li> | 
            <li>Privacy Policy</li> | 
            <li>Terms and Conditions</li>
          </ul>          
          <small>
            copyright &copy; 2019
            <br />
            All rights Reserved
          </small>
        </div>
      </div>
    )
  }
}

